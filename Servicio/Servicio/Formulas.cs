﻿using System;

namespace Servicio
{
    public class Formulas : IFormulas
    {
        public string AreaTriangulo(int b, int h)
        {
            var area = (b * h) / 2;

            return "El área es " + area;
        }

        public string PerimetroTriangulo(int a, int b, int c)
        {
            var perimetro = a + b + c;

            return "El perímetro es " + perimetro;
        }

        public string Polinomio(int a, int b, int c)
        {
            var polinomio = a + "x^2 + " + b + "x + " + c;

            return "El polinomio es " + polinomio;
        }

        public string Potencia(int a, int b)
        {
            var potencia = Math.Pow(a, b);

            return "Él número " + a + "^" + b + " es " + potencia;
        }
    }
}
