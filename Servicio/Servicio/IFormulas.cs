﻿using System.ServiceModel;

namespace Servicio
{    
    [ServiceContract]
    public interface IFormulas
    {
        [OperationContract]
        string PerimetroTriangulo(int a, int b, int c);

        [OperationContract]
        string AreaTriangulo(int b, int h);

        [OperationContract]
        string Potencia(int a, int b);

        [OperationContract]
        string Polinomio(int a, int b, int c);
    }    
}
